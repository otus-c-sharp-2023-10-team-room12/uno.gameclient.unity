using System;
using System.IO;
using System.Text;
using UnityEngine;
using VContainer.Unity;

namespace Common
{
    public class GameLogs : IInitializable, IDisposable
    {
        private readonly bool _gameLogsWarning;
        private readonly bool _showStackTrace;
        private const string LogsFileName = "logs.txt";

        private readonly StringBuilder _stringBuilder;

        private string _logsPath;

        public GameLogs(bool gameLogsWarning, bool showStackTrace)
        {
            _gameLogsWarning = gameLogsWarning;
            _showStackTrace = showStackTrace;
            _stringBuilder = new StringBuilder(100);
        }

        public void Dispose()
        {
            Application.logMessageReceived -= OnLogsMessageRecievedHandler;
        }

        public void Initialize()
        {
            var directory = Directory.GetCurrentDirectory();
            _logsPath = Path.Combine(directory, LogsFileName);

            if (File.Exists(_logsPath))
            {
                File.Delete(_logsPath);
            }

            Application.logMessageReceived += OnLogsMessageRecievedHandler;
        }

        private void OnLogsMessageRecievedHandler(string condition, string stacktrace, LogType type)
        {
            if ((type == LogType.Warning || type == LogType.Assert) && !_gameLogsWarning) return;

            _stringBuilder.AppendLine($"[{type}] {condition}");
            if (_showStackTrace && !string.IsNullOrEmpty(stacktrace))
            {
                _stringBuilder.AppendLine($"  Stack: {stacktrace}");
            }

            WriteToLogsFile(_stringBuilder.ToString());
            _stringBuilder.Clear();
        }

        private void WriteToLogsFile(string logsMessage)
        {
            try
            {
                File.AppendAllText(_logsPath, logsMessage, Encoding.UTF8);
            }
            catch (Exception e)
            {
            }
        }
    }
}