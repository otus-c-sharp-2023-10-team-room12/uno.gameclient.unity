using Common;
using Controllers;
using Data;
using Settings;
using UI;
using UnityEngine;
using UnityEngine.Serialization;
using VContainer;
using VContainer.Unity;

namespace Scopes
{
    public class ProjectScope : LifetimeScope
    {
        [SerializeField] private MessageBox _messageBoxPrefab;

        [SerializeField] private GameServerConfig _serverConfig;

        [SerializeField] private bool _gameLogsWarning;
        [SerializeField] private bool _gameLogsStackTrace;

        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterComponentInNewPrefab(_messageBoxPrefab, Lifetime.Singleton).DontDestroyOnLoad();

            builder.Register<NavigationController>(Lifetime.Singleton);

            builder.Register<CurrentGameSessionData>(Lifetime.Singleton);

            builder.RegisterEntryPoint<GameSessionController>(Lifetime.Singleton);

            builder.RegisterEntryPoint<GameLogs>().WithParameter(_gameLogsWarning).WithParameter(_gameLogsStackTrace);

            builder.Register<ServerLogger>(Lifetime.Scoped).As<IServerLogger>().WithParameter(_serverConfig.EnableLogs);
            builder.RegisterEntryPoint<GameServerApi>().As<IGameServerApi>().WithParameter(_serverConfig);
        }
    }
}