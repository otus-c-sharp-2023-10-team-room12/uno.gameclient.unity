using UnityEngine;
using Uno.CommonLibrary.Rooms;

namespace Data
{
    public class CurrentGameSessionData
    {
        public RoomData CurrentPlayRoom { get; set; }
        
        public RoomPlayerData MyPlayer { get; set; }

        public bool IsConnectedAndPlaying()
        {
            return CurrentPlayRoom != null && MyPlayer.Status == RoomPlayerData.ConnectionStatus.Connected;
        }

        public void SetDisconnected()
        {
            if (CurrentPlayRoom != null)
            {
                MyPlayer.Status = RoomPlayerData.ConnectionStatus.Disconnected;
            }
        }

        public void SetConnected()
        {
            if (CurrentPlayRoom != null)
            {
                MyPlayer.Status = RoomPlayerData.ConnectionStatus.Connected;
            }
        }
        
        
        public CurrentGameSessionData()
        {
            MyPlayer = new RoomPlayerData()
            {
                Status = RoomPlayerData.ConnectionStatus.None,
                UserName = "TestPlayer",
                PlayerId = SystemInfo.deviceUniqueIdentifier
            };
        }
    }
}