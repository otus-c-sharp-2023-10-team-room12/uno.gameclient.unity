namespace Data
{
    public enum CardColorType
    {
        Red,
        Green,
        Yellow,
        Blue
    }
    
    public enum ConnectionStatus
    {
        None = 0,
        Connected = 1,
        Disconnected = 2
    } 
    
}