using Controllers.Services;
using Cysharp.Threading.Tasks;

namespace Controllers
{
    public interface IGameServerApi
    {
        bool IsConnectedToHub { get; }

        T GetService<T>() where T : BaseService;
        
        public bool IsInitialized { get; }

        UniTask InitializeAsync();
    }
}