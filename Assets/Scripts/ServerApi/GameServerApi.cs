using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Controllers.Services;
using Cysharp.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Settings;
using UI;
using Uno.CommonLibrary.SignalR;
using VContainer.Unity;

namespace Controllers
{
    public sealed class GameServerApi : IGameServerApi, IInitializable, IDisposable
    {
        public event Action<ClientMessage> OnHubDisconnected;

        private readonly GameServerConfig _serverConfig;
        private readonly IServerLogger _serverLogger;
        private readonly MessageBox _messageBox;
        private readonly Dictionary<Type, BaseService> _services;

        private HubConnection _connection;
        public bool IsConnectedToHub => _connection != null && _connection.State == HubConnectionState.Connected;

        public bool IsInitialized => _isInitialized;

        private bool _isInitialized;

        public GameServerApi(GameServerConfig config, IServerLogger serverLogger, MessageBox messageBox)
        {
            _serverConfig = config;
            _serverLogger = serverLogger;
            _messageBox = messageBox;
            _services = new Dictionary<Type, BaseService>();
        }

        public T GetService<T>() where T : BaseService
        {
            var type = typeof(T);
            return (T)_services[type];
        }


        public void Initialize()
        {
            _serverConfig.Initialize(ConfigLoaded);
        }

        private void ConfigLoaded()
        {
            _services.Add(typeof(LoginService), new LoginService(_serverConfig.LoginServiceEndPoint, _serverLogger));
            _services.Add(typeof(RoomsService), new RoomsService(_serverConfig.RoomServiceEndPoint, _serverLogger));
            _services.Add(typeof(GamePlayService),
                new GamePlayService(_serverConfig.GamePlayServiceEndPoint, _serverLogger));
        }

        public async UniTask InitializeAsync()
        {
            if (_isInitialized) return;
            await UniTask.WaitUntil(() => _serverConfig.ConfigsLoaded);
            //connect to Hub
            await ConnectAsync();

            _isInitialized = true;
        }

        private async UniTask ConnectAsync()
        {
            var hubEndPoint = _serverConfig.SignalREndPoint;
            _connection ??= new HubConnectionBuilder().WithUrl(hubEndPoint.GetUrl()).WithAutomaticReconnect().Build();

            _connection.Closed -= OnConnectionClosedHandler;
            _connection.Closed += OnConnectionClosedHandler;

            _connection.Reconnected -= OnReconnectedHandler;
            _connection.Reconnected += OnReconnectedHandler;

            while (_connection.State != HubConnectionState.Connected)
            {
                try
                {
                    if (_connection.State == HubConnectionState.Connecting)
                    {
                        await UniTask.Delay(_serverConfig.HubReconnectDelaySeconds);
                    }

                    _serverLogger.Log($"connection start: {hubEndPoint.GetUrl()}");
                    await _connection.StartAsync();
                    _serverLogger.Log($"connection success: {hubEndPoint.GetUrl()}");
                }
                catch (Exception e)
                {
                    _serverLogger.LogError(this, "connection to hub error", e);
                    _messageBox.ShowAlignedTop("Connection Error", $"{e.Message}:\n{e.StackTrace}", () =>
                    {
                        UniTask.Action(async () =>
                        {
                            await UniTask.Delay(TimeSpan.FromSeconds(_serverConfig.HubReconnectDelaySeconds));
                            await ConnectAsync();
                        }).Invoke();
                    });
                    break;
                }
            }
        }


        private Task OnReconnectedHandler(string arg)
        {
            _serverLogger.Log($"reconnected:{arg}");
            return Task.CompletedTask;
        }

        private Task OnConnectionClosedHandler(Exception arg)
        {
            _serverLogger.LogError(this, "hub close connection", arg);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _connection.Reconnected -= OnReconnectedHandler;
            _connection.Closed -= OnConnectionClosedHandler;
            if (_connection.State == HubConnectionState.Connected)
            {
                UniTask.Void(async () => await _connection.DisposeAsync());
            }
        }
    }
}