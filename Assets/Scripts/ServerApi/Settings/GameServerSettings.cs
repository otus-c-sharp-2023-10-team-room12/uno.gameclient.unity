using System;
using System.IO;
using System.Text;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using NaughtyAttributes;
using Settings.Data;
using UnityEngine;
using UnityEngine.Serialization;

namespace Settings
{
    // [CreateAssetMenu(fileName = "GameServerConfig", menuName = "Create GameServer Config", order = 0)]
    public class GameServerConfig : ScriptableObject
    {
        [SerializeField] private bool _enableLogs;

        [SerializeField] private EndPoint _roomServiceEndPoint;

        [SerializeField] private EndPoint _loginServiceEndPoint;

        [SerializeField] private EndPoint _gamePlayServiceEndPoint;

        [SerializeField] private EndPoint _signalREndPoint;

        [SerializeField] private string _configFileName;

        [SerializeField] private int _hubReconnectDelaySeconds;

        public bool IsInitialized => _isInitialized;

        public bool EnableLogs => Debug.isDebugBuild && _enableLogs;

        public EndPoint SignalREndPoint => _signalREndPoint;

        public EndPoint RoomServiceEndPoint => _roomServiceEndPoint;

        public int HubReconnectDelaySeconds => _hubReconnectDelaySeconds;

        public EndPoint LoginServiceEndPoint => _loginServiceEndPoint;

        public EndPoint GamePlayServiceEndPoint => _gamePlayServiceEndPoint;

        public bool ConfigsLoaded => _configsLoaded;

        [NonSerialized] private bool _isInitialized;

        [NonSerialized] private bool _configsLoaded;

        public void Initialize(Action callback)
        {
            if (_isInitialized) return;
            _isInitialized = true;
            LoadAsync(callback).Forget();
        }

        private async UniTaskVoid LoadAsync(Action callback = null)
        {
            var path = Path.Combine(Application.streamingAssetsPath, _configFileName);
            try
            {
                var json = await File.ReadAllTextAsync(path, Encoding.UTF8);
                if (!string.IsNullOrEmpty(json))
                {
                    JsonUtility.FromJsonOverwrite(json, this);
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"Error to load config from:{path}\n{e.Message}");
            }

            _configsLoaded = true;
            callback?.Invoke();
        }

        [UsedImplicitly]
        [Button]
        private void SaveToJson()
        {
            if (string.IsNullOrEmpty(_configFileName)) return;
            var json = JsonUtility.ToJson(this);
            var path = Path.Combine(Application.streamingAssetsPath, _configFileName);

            try
            {
                File.WriteAllText(path, json, Encoding.UTF8);
#if UNITY_EDITOR
                UnityEditor.AssetDatabase.SaveAssets();
                UnityEditor.AssetDatabase.Refresh();
#endif
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
    }
}