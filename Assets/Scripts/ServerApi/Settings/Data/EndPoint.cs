using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Settings.Data
{
    [JsonObject(MemberSerialization.Fields)]
    [Serializable]
    public class EndPoint
    {
        [SerializeField] private string _devUrl;
        [SerializeField] private string _productionUrl;
       
        public bool HasUrl()
        {
            return !string.IsNullOrEmpty(_devUrl) && !string.IsNullOrEmpty(_productionUrl);
        }

        public string GetUrl(string methodName)
        {
            return $"{GetUrl()}/{methodName}";
        }

        public string GetUrl()
        {
            return Debug.isDebugBuild ? _devUrl : _productionUrl;
        }
    }
}