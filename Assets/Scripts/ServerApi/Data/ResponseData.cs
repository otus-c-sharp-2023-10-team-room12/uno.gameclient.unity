using UnityEngine.Networking;

namespace Controllers
{
    public class ResponseData<T> : ResponseData
    {
        public readonly T data;

        public ResponseData(T data, int code, string body) : base(code, body)
        {
            this.data = data;
        }
        
        
    }

    public class ResponseData
    {
        public const int OkCode = 200;
        public const int ErrorCode = -1;

        public readonly int Code;
        public readonly string Body;

        public ResponseData(int code, string body)
        {
            Body = body;
            Code = code;
        }

        public bool IsOk()
        {
            return Code == OkCode;
        }

        public override string ToString()
        {
            return $"ResponseCode:{Code},body:{Body}";
        }
    }
}