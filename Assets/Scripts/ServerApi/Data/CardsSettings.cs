using System;
using Data;
using UnityEngine;

namespace Settings
{
    [Serializable]
    public class CardViewInfo
    {
        [SerializeField] private int _id;
        public Sprite card;
        [SerializeField] private CardColorType colorType;

        public int ID => _id;

        public CardColorType ColorType => colorType;
    }

    [Serializable]
    public class CardsCatalog
    {
        
    }
    
    
    [CreateAssetMenu(fileName = "Cards", menuName = "Create Cards", order = 0)]
    public class CardsSettings : ScriptableObject
    {
        [SerializeField] private CardViewInfo[] _cards;


        public CardViewInfo[] Cards => _cards;
    }
}