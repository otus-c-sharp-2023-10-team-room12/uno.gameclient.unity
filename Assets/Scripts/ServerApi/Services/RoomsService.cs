using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Settings.Data;
using UnityEngine.Networking;
using Uno.CommonLibrary.Rooms;

namespace Controllers.Services
{
    public class RoomsService : BaseService
    {
        public RoomsService(EndPoint endPoint, IServerLogger logger) : base(endPoint, logger)
        {
        }

        public async UniTask<ResponseData<IReadOnlyList<RoomData>>> GetAllActiveRoomsAsync()
        {
            var response = await GetRequestAsync("GetActiveRooms");

            IReadOnlyList<RoomData> rooms = null;
            if (!response.IsOk()) return new ResponseData<IReadOnlyList<RoomData>>(rooms, response.Code, response.Body);
            try
            {
                rooms = JsonConvert.DeserializeObject<IReadOnlyList<RoomData>>(response.Body);
                return new ResponseData<IReadOnlyList<RoomData>>(rooms, response.Code, response.Body);
            }
            catch (Exception e)
            {
                return new ResponseData<IReadOnlyList<RoomData>>(rooms, ResponseData.ErrorCode, e.Message);
            }
        }

        public async UniTask<ResponseData> ClearAllRoomsAsync()
        {
            return await GetRequestAsync("ClearAllRooms");
        }

        public async UniTask<ResponseData<RoomData>> CreateRoomRequest([NotNull] CreateRoomRequest createRoomRequest)
        {
            var response = await PostRequestAsync("CreateRoom", JsonConvert.SerializeObject(createRoomRequest));
            if (response.IsOk())
            {
                var roomData = JsonConvert.DeserializeObject<RoomData>(response.Body);
                return new ResponseData<RoomData>(roomData, response.Code, response.Body);
            }

            return new ResponseData<RoomData>(null, response.Code, response.Body);
        }

        public async UniTask<ResponseData> DisconnectFromRoomAsync(string roomId, string playerId)
        {
            return await PostRequestAsync("DisconnectFromRoom",
                new List<IMultipartFormSection>()
                    { new MultipartFormDataSection($"roomId={roomId}&playerId:{playerId}") });
        }

        public async UniTask<ResponseData> JoinToRoomAsync(JoinToRoomRequest roomRequest)
        {
            return await PostRequestAsync("JoinToRoom", JsonConvert.SerializeObject(roomRequest));
        }
    }
}