using Cysharp.Threading.Tasks;
using Settings.Data;

namespace Controllers.Services
{
    public class LoginService:BaseService
    {
        public LoginService(EndPoint endPoint, IServerLogger serverLogger) : base(endPoint, serverLogger)
        {
        }

        public async UniTask<ResponseData> LoginAsync(string userName, string password)
        {
          
            await UniTask.Yield();

            //TODO:implement login service
            return new ResponseData(ResponseData.OkCode, $"{userName}:{password}");
        }
    }
}