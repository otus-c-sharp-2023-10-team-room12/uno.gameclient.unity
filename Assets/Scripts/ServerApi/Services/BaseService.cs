using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Settings.Data;
using UnityEngine.Networking;

namespace Controllers.Services
{
    public abstract class BaseService
    {
        protected const int OkCode = 200;

        private readonly EndPoint _endPoint;
        private readonly IServerLogger _serverLogger;
        
        protected BaseService(EndPoint endPoint,IServerLogger serverLogger)
        {
            this._endPoint = endPoint;
            _serverLogger = serverLogger;
        }


        protected async UniTask<ResponseData> PostRequestAsync(string methodName, List<IMultipartFormSection> data)
        {
            try
            {
                using var request = UnityWebRequest.Post(_endPoint.GetUrl(methodName), data);
                await request.SendWebRequest().ToUniTask();
                _serverLogger.LogResponse(this, (int)request.responseCode, request.downloadHandler.text,
                    request.error);

                return CreateResponseData(request);
            }
            catch (Exception e)
            {
                _serverLogger.LogError(this, $"method:{methodName}", e);
                return new ResponseData(-1, e.Message);
            }
        }

        protected async UniTask<ResponseData> PostRequestAsync(string methodName, string jsonData)
        {
            try
            {
                using var request = UnityWebRequest.Post(_endPoint.GetUrl(methodName), jsonData, "application/json");
                await request.SendWebRequest().ToUniTask();
                _serverLogger.LogResponse(this, (int)request.responseCode, request.downloadHandler.text, request.error);

                return CreateResponseData(request);
            }
            catch (Exception e)
            {
                _serverLogger.LogError(this, $"method:{methodName}", e);
                return new ResponseData(-1, e.Message);
            }
        }

        protected async UniTask<ResponseData> GetRequestAsync(string methodName)
        {
            try
            {
                using var request = UnityWebRequest.Get(_endPoint.GetUrl(methodName));
                await request.SendWebRequest();
                _serverLogger.LogResponse(this, (int)request.responseCode, request.downloadHandler.text, request.error);

                return CreateResponseData(request);
            }
            catch (Exception e)
            {
                _serverLogger.LogError(this, $"method:{methodName}", e);
                return new ResponseData(-1, e.Message);
            }
        }

        protected virtual ResponseData CreateResponseData(UnityWebRequest request)
        {
            if (request.result == UnityWebRequest.Result.Success)
            {
                return new ResponseData((int)request.responseCode, request.downloadHandler.text);
            }

            return new ResponseData((int)request.responseCode, request.error);
        }
    }
}