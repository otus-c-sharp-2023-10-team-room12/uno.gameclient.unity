using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Controllers
{
    public interface IServerLogger
    {
        void LogResponse(object context, int code, string body, string error);

        void LogError(object context, string message, Exception exception = null);

        void Log(string message);
    }

    public class ServerLogger : IServerLogger
    {
        private readonly bool _enabled;

        public ServerLogger(bool enabled)
        {
            _enabled = enabled;
        }


        public void LogResponse(object context, int code, string body, string error)
        {
            if (!_enabled) return;
            Debug.Log($"{context.GetType().Name}: Response Code:{code},body:{body} error:{error}");
        }

        public void LogError(object context, string message, Exception exception = null)
        {
            if (!_enabled) return;
            Debug.LogError($"{context.GetType().Name} - {message}");
            if (exception != null)
            {
                Debug.LogException(exception);
            }
        }

        public void Log(string message)
        {
            if (!_enabled) return;
            Debug.Log(message);
        }
    }
}