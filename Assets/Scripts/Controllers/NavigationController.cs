using System;
using UnityEngine.SceneManagement;

namespace Controllers
{
    public class NavigationController
    {
        public event Action OnExitFromGameEvent;
        public event Action OnLogoutEvent;

        public event Action OnGameLoadedEvent;
        
        public void LoadGameScreen()
        {
            if(SceneManager.GetActiveScene().name == "GameScene")return;
            SceneManager.LoadScene("GameScene");
            OnGameLoadedEvent?.Invoke();
        }

        public void LoadLobbyScreen()
        {
            if(SceneManager.GetActiveScene().name == "LobbyScene")return;
            SceneManager.LoadScene("LobbyScene");
            OnExitFromGameEvent?.Invoke();
        }

        public void LoadLoginScreen()
        {
            if(SceneManager.GetActiveScene().name == "LoginScene")return;
            SceneManager.LoadScene("LoginScene");
            OnLogoutEvent?.Invoke();
        }
    }
}