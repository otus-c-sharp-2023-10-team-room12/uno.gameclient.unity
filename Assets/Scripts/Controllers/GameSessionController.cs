using System;
using Data;
using VContainer;
using VContainer.Unity;

namespace Controllers
{
    public class GameSessionController:IInitializable,IDisposable,IStartable
    {
        private IGameServerApi _serverApi;
        private NavigationController _navigation;
        private CurrentGameSessionData _sessionData;

        private bool _quitResult;
        
        public GameSessionController(IGameServerApi serverApi, NavigationController navigationController,
            CurrentGameSessionData sessionData)
        {
            _sessionData = sessionData;
            _navigation = navigationController;
            _serverApi = serverApi;
        }
        
        
        private bool OnAppWantsToQuitHandler()
        {
            return _quitResult;
          
        }

        [Inject]
        private void Construct(CurrentGameSessionData sessionData, IGameServerApi serverApi,
            NavigationController navigationController)
        {
            _sessionData = sessionData;
            _serverApi = serverApi;
            _navigation = navigationController;
        }

        
        private void OnApplicationPause(bool pauseStatus)
        {
            if (!pauseStatus)
            {
                _sessionData.SetConnected();
            }
            else
            {
                _sessionData.SetDisconnected();
            }
        }

        public void Dispose()
        {
           
        }

        public void Initialize()
        {
            
        }
        
        public void Start()
        {
            
        }
    }
}