using System;
using Controllers.Services;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;
using VContainer;
using VContainer.Unity;

namespace Controllers
{
    [RequireComponent(typeof(UIDocument))]
    public class LoginController : MonoBehaviour
    {
        private TextField _loginField;
        private TextField _passwordField;
        private Button _loginButton;

        [SerializeField] private UIDocument _uiDocument;
        private IGameServerApi _serverApi;
        private LifetimeScope _parent;
        
        private void OnEnable()
        {
            var rootElement = _uiDocument.rootVisualElement;

            _loginField = rootElement.Q<TextField>("loginField");
            _passwordField = rootElement.Q<TextField>("passwordField");
            _loginButton = rootElement.Q<Button>("loginButton");

            _loginButton.clicked += OnLoginClickedHandler;
        }

        private void OnDisable()
        {
            _loginButton.clicked -= OnLoginClickedHandler;
        }

        [Inject]
        private void Construct(IGameServerApi serverApi)
        {
            _serverApi = serverApi;
        }

        private void OnLoginClickedHandler()
        {
            //TODO: authorization logic
            _loginButton.SetEnabled(false);
            LoginAsync().Forget();
        }

        private async UniTaskVoid LoginAsync()
        {
            if (!_serverApi.IsInitialized)
            {
                await _serverApi.InitializeAsync();
            }

            var loginService = _serverApi.GetService<LoginService>();

            await loginService.LoginAsync(_loginField.value,_passwordField.value);

            await SceneManager.LoadSceneAsync("LobbyScene");
        }
    }
}