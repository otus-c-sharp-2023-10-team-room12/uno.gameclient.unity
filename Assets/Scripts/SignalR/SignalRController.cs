using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SignalR
{
    public class SignalRController : MonoBehaviour
    {
        [SerializeField] private ScrollRect _scroller;
        [SerializeField] private TMP_InputField _messageField;
        [SerializeField] private TextMeshProUGUI _text;

        [SerializeField] private float connectionDelay;
        [SerializeField] private string _serverUrl;

        private HubConnection _connection;

        private readonly Queue<string> _messagesQueue = new Queue<string>(10);

        private void Awake()
        {
            _messageField.onEndEdit.AddListener(OnEndEditHandler);
        }

        private void OnEndEditHandler(string text)
        {
            SendMessageAsync().Forget();
        }

        private async void Start()
        {
            _connection = await ConnectToSignalR();
        }

        public void SendMessageClick()
        {
            SendMessageAsync().Forget();
        }

        private async UniTaskVoid SendMessageAsync()
        {
            try
            {
                if (string.IsNullOrEmpty(_messageField.text)) return;
                await _connection.SendAsync("SendMessage", _messageField.text);
                _messageField.text = "";
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        private async UniTask<HubConnection> ConnectToSignalR()
        {
            var connection = new HubConnectionBuilder().WithUrl(_serverUrl).WithAutomaticReconnect().Build();


            Debug.Log("connection handle created");
            
            //подписываемся на сообщение от хаба, чтобы проверить подключение
            connection.On<string>("OnMessage", OnMessage);
            connection.On<string, string>("OnReceieved",
                (user, message) => LogAsync($"{user}: {message}").Forget());

            while (connection.State != HubConnectionState.Connected)
            {
                
                try
                {
                    if (connection.State == HubConnectionState.Connecting)
                    {
                        await UniTask.Delay(TimeSpan.FromSeconds(connectionDelay));
                        continue;
                    }

                    Debug.Log("start connection");
                    await connection.StartAsync();
                    Debug.Log("connection finished");
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }

            return connection;
        }

        private void OnMessage(string message)
        {
            _messagesQueue.Enqueue(message + "\n");
        }

        private async UniTaskVoid LogAsync(string s)
        {
            _messagesQueue.Enqueue(s + "\n");
            await UniTask.Yield();
        }

        private void Update()
        {
            if (_messagesQueue.Count > 0)
            {
                _text.text += _messagesQueue.Dequeue();
                _scroller.verticalNormalizedPosition = 0f;
            }
        }
    }
}