using Settings;
using UnityEditor;

namespace Editor
{
    public static class EditorTools
    {
        public const string MenuString = "UnoGame";

        [MenuItem(MenuString + "/Select GameServerApi Config")]
        public static void SelectGameServerConfig()
        {
            Selection.activeObject =
                AssetDatabase.LoadAssetAtPath("Assets/Scripts/ServerApi/Assets/GameServerConfig.asset", typeof(GameServerConfig));
        }
    }
}