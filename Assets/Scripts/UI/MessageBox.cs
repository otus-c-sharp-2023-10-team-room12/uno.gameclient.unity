using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace UI
{
    public class MessageBox : MonoBehaviour
    {
        [SerializeField] private UIDocument _uiDocument;

        private Button _closeButton;

        private Label _tittle;
        private Label _message;
        private VisualElement _rootElement;

        private Action _closeCallback;

        private void Start()
        {
            _rootElement = _uiDocument.rootVisualElement;
            _closeButton = _rootElement.Q<Button>("okButton");
            _message = _rootElement.Q<Label>("message");
            _tittle = _rootElement.Q<Label>("title");

            _tittle.text = "";
            _message.text = "";

            _closeButton.clicked += OnCloseButtonHandler;

            _rootElement.style.display = DisplayStyle.None;
        }

        private void OnDestroy()
        {
            if (_closeButton != null)
            {
                _closeButton.clicked -= OnCloseButtonHandler;
            }
        }

        private void OnCloseButtonHandler()
        {
            _rootElement.style.display = DisplayStyle.None;
            _closeCallback?.Invoke();
            _closeCallback = null;
        }

        public void ShowAlignedTop(string title, string message, Action closeCallback = null)
        {
            _message.style.unityTextAlign = new StyleEnum<TextAnchor>(TextAnchor.UpperLeft);
            _tittle.text = title;
            _message.text = message;
            _rootElement.style.display = DisplayStyle.Flex;
            _closeCallback = closeCallback;
        }

        public void Show(string title, string message, Action closeCallback = null)
        {
            _message.style.unityTextAlign = new StyleEnum<TextAnchor>(TextAnchor.MiddleCenter);
            _tittle.text = title;
            _message.text = message;
            _rootElement.style.display = DisplayStyle.Flex;
            _closeCallback = closeCallback;
        }
    }
}