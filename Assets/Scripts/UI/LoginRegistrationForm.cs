using System;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LoginRegistrationForm : MonoBehaviour
    {
        [NotNull] public TMP_InputField loginField;
        [NotNull] public TMP_InputField passField;

        [NotNull] public Button loginButton;

        private void OnDestroy()
        {
            loginButton.onClick.RemoveAllListeners();
        }
    }
}