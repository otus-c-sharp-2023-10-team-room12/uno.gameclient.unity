using System;
using System.Collections.Generic;
using Controllers;
using Controllers.Services;
using Cysharp.Threading.Tasks;
using Data;
using UnityEngine;
using UnityEngine.UIElements;
using Uno.CommonLibrary.Rooms;
using VContainer;

namespace UI
{
    [RequireComponent(typeof(UIDocument))]
    public class LobbyUIController : MonoBehaviour
    {
        [SerializeField] private UIDocument _lobbyUI;

        private IGameServerApi _serverApi;
        private NavigationController _navigation;
        private CurrentGameSessionData _sessionData;
        private MessageBox _messageBox;

        private RoomsService _roomsService;
        private IReadOnlyList<RoomData> _currentRooms;

        private TextField _roomNameField;
        private SliderInt _totalPlayersSlider;

        private Button _clearAllRoomsButton;
        private Button _createRoomButton;
        private Button _joinButton;

        private ListView _listView;
        private List<string> _roomNames;


        private void Awake()
        {
            _roomNames = new List<string>(100);
        }


        [Inject]
        public void Construct(IObjectResolver objectResolver)
        {
            Debug.Log("constructed");
            _serverApi = objectResolver.Resolve<IGameServerApi>();
            _navigation = objectResolver.Resolve<NavigationController>();
            _sessionData = objectResolver.Resolve<CurrentGameSessionData>();
            _messageBox = objectResolver.Resolve<MessageBox>();

            var rootElement = _lobbyUI.rootVisualElement;

            _clearAllRoomsButton = rootElement.Q<Button>("clearAllRoomsButton");
            _createRoomButton = rootElement.Q<Button>("createRoomButton");

            _roomNameField = rootElement.Q<TextField>("roomName");
            _totalPlayersSlider = rootElement.Q<SliderInt>("totalPlayers");

            _clearAllRoomsButton.clickable = new Clickable(OnClearRoomHandler);
            _createRoomButton.clicked += OnCreateRoomClickedHandler;

            var listHolder = rootElement.Q<VisualElement>("listViewHolder");

            _joinButton = rootElement.Q<Button>("joinButton");
            _joinButton.clicked += OnJoinClickHandler;

            _listView = new ListView(_roomNames, 30, () => new Label()
                {
                    style = { unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold) }
                },
                (element, i) => ((Label)element).text = _roomNames[i]);
            _listView.style.flexBasis = new StyleLength(StyleKeyword.Auto);
            _listView.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);

            _listView.itemsSource = _roomNames;
            _listView.Q<ScrollView>().mouseWheelScrollSize = 50;
            // _listView.itemsChosen += OnItemChooseHandler;
            _listView.selectionChanged += OnSelectionChangedHandler;
            _listView.showBorder = true;
            _listView.style.flexGrow = new StyleFloat(1f);
            listHolder.Add(_listView);

            InitializeAsync().Forget();
        }

        private async UniTaskVoid InitializeAsync()
        {
            if (!_serverApi.IsInitialized)
            {
                await _serverApi.InitializeAsync();
            }

            _roomsService = _serverApi.GetService<RoomsService>();
            await LoadRoomsAsync();
        }

        private void OnClearRoomHandler()
        {
            UniTask.Action(async () =>
            {
                var response = await _roomsService.ClearAllRoomsAsync();

                if (response.IsOk())
                {
                    _currentRooms = null;
                    _roomNames.Clear();
                    _listView.RefreshItems();

                    _messageBox.Show("Clear Rooms", response.Body);
                }
                else
                {
                    _messageBox.Show("Clear Rooms", response.Body);
                }
            }).Invoke();
        }

        private void OnCreateRoomClickedHandler()
        {
            _createRoomButton.SetEnabled(false);
            UniTask.Action(async () =>
            {
                _sessionData.MyPlayer.Status = RoomPlayerData.ConnectionStatus.Connected;

                var response = await _roomsService.CreateRoomRequest(new CreateRoomRequest()
                {
                    Name = _roomNameField.text,
                    Creator = _sessionData.MyPlayer,
                    MaxPlayers = _totalPlayersSlider.value
                });

                if (response.IsOk())
                {
                    _sessionData.CurrentPlayRoom = response.data;
                    _navigation.LoadGameScreen();
                }
                else
                {
                    _messageBox.Show("Create Room Error", response.Body);
                }

                _createRoomButton.SetEnabled(true);
            }).Invoke();
        }

        private void OnSelectionChangedHandler(IEnumerable<object> element)
        {
            _sessionData.CurrentPlayRoom = _listView.selectedIndex < _currentRooms.Count
                ? _currentRooms[_listView.selectedIndex]
                : null;
        }


        private async UniTask LoadRoomsAsync()
        {
            var response = await _roomsService.GetAllActiveRoomsAsync();

            if (!response.IsOk()) return;

            _currentRooms = response.data;

            _joinButton.SetEnabled(false);
            _sessionData.CurrentPlayRoom = null;
            _roomNames.Clear();

            foreach (var roomData in _currentRooms)
            {
                _roomNames.Add($"Name: {roomData.Name} connected players: {roomData.PlayersConnected.Count}");
            }

            _listView.RefreshItems();
            _joinButton.SetEnabled(_roomNames.Count > 0);
        }


        private void OnDestroy()
        {
            if (_listView != null)
            {
                _listView.selectionChanged -= OnSelectionChangedHandler;
            }

            if (_joinButton != null)
            {
                _joinButton.clicked -= OnJoinClickHandler;
            }

            if (_createRoomButton != null)
            {
                _createRoomButton.clicked -= OnCreateRoomClickedHandler;
            }
        }

        private void OnJoinClickHandler()
        {
            JoinToRoomAsync().Forget();
        }

        private async UniTaskVoid JoinToRoomAsync()
        {
            if (_sessionData.CurrentPlayRoom != null)
            {
                _joinButton.SetEnabled(false);
                var response = await _roomsService.JoinToRoomAsync(new JoinToRoomRequest()
                {
                    PlayerData = _sessionData.MyPlayer,
                    RoomId = _sessionData.CurrentPlayRoom.Id
                });

                if (response.IsOk())
                {
                    _navigation.LoadGameScreen();
                }
                else
                {
                    _messageBox.Show("Joint To Room Error", response.Body);
                    _joinButton.SetEnabled(true);
                }
            }
        }
    }
}